## Buat dokumen spreadsheet
- buat dokumen spreadsheet isikan seperti ini `nomor_rack nama_barang kode_barang gambar` pada row pertama
- lalu salin ID spreadsheet nya dan pastekan di script nya pada variable sheetID


## Buat project di google app script
- Buat sebuah project di google app script
- lalu isikan dengan file scipt.gs yang berada di folder script
- klik Deploy lalu klik new Deployment, pada development type pilih WebApp
- pada Execute as pilih Me, dan pada Who has access pilih Anyone lalu klik deploy
- salin URL nya lalu paste di file Config.java pada variabel url

## Import project di Android studio
- silahkan buka Android studio lalu open project dengan cara klik `File` - `Open`
- cari folder project ini klik OK
- silahkan tunggu sampai selesai mendownload semua dependencies nya
- silahkan edit sesuka hati

apabila ada kekurangan mohon dimaafkan, dan kalau menemukan bug silahkan laporkan di bagian issue terimakasih semoga bermanfaat
