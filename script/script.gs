// by nzaa modified Selasa, 13 Juni 2023
// semoga bermanfaat :D

// ID sheet
var sheetID = "";
// object spreadsheet
const spreadSheet = SpreadsheetApp.openById(sheetID);
// ambil sheet pertama
const sheet = spreadSheet.getSheets()[0];


// hit api oleh client / android
function doPost(e) {
  const data = ContentService.createTextOutput(getDatas(e.parameter.nomor_rack));
  data.setMimeType(ContentService.MimeType.JSON);
  return data;
}


function getDatas(e) {
  // ambil range
  const range = sheet.getDataRange();
  // ambil yg ada datanya
  const val = range.getValues();

  // tampung array data / row nya
  let data = [];

  // looping row brader
  for (let i = 1; i < val.length; i++) {
    // tampung object data
    let perRow = {};

    // filter apabila ada kalo ga ya lanjut tanpa filter
    if(e != null && e != "" && val[i][0] != e) {
      continue
    }

      // mapping data rownya
      perRow.kode_barang = val[i][0];
      perRow.nama_barang = val[i][1];
      perRow.nomor_rack = val[i][2];
      perRow.gambar = val[i][3];

      // push data
      data.push(perRow);
    
  }
  // buat nge-debug aja
  console.log(data)
  
  // tampilin ke client
  return JSON.stringify({results: data})
}
