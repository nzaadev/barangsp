package com.nzaadev.barangsp;

import static com.nzaadev.barangsp.Config.url;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Utils {


    public static void getData (Context context, String dataz, DataResponse data) {
        RequestQueue queue = Volley.newRequestQueue(context);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        data.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                data.onError(error.getMessage());
            }
        });

        // Add the request to the RequestQueue.

        queue.add(stringRequest);

    }

    public static void postData (Context context, String name, String image, String code, DataResponse data) {
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("action", "insert");
            jsonBody.put("id", String.valueOf(System.currentTimeMillis()));
            jsonBody.put("name", name);
            jsonBody.put("image", image);
            jsonBody.put("code", code);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        final String mRequestBody = jsonBody.toString();

        RequestQueue queue = Volley.newRequestQueue(context);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        data.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                data.onError(error.getMessage());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "text/plain; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                return mRequestBody.getBytes(StandardCharsets.UTF_8);
            }
        };

        // Add the request to the RequestQueue.

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

    public static void updateData (Context context, String id, String name, String image, String code, DataResponse data) {
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("action", "update");
            jsonBody.put("id", id);
            jsonBody.put("name", name);
            jsonBody.put("image", image);
            jsonBody.put("code", code);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        final String mRequestBody = jsonBody.toString();

        RequestQueue queue = Volley.newRequestQueue(context);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        data.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                data.onError(error.getMessage());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "text/plain; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                return mRequestBody.getBytes(StandardCharsets.UTF_8);
            }
        };

        // Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
        queue.add(stringRequest);

    }

}
