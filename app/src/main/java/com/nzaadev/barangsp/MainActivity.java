package com.nzaadev.barangsp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemSelectedListener {


    private RecyclerView rv;
    private BarangAdapter barangAdapter;
    private ProgressBar progressBar;
    private SwipeRefreshLayout srl;
    private Spinner spinner;
    private TextView empty;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(Login.db, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        spinner = findViewById(R.id.spinner);
        empty = findViewById(R.id.empty);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.array,
                android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        srl = findViewById(R.id.srl);
        srl.setOnRefreshListener(this);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        barangAdapter = new BarangAdapter();
        rv = findViewById(R.id.rv);
        rv.setLayoutManager(new StaggeredGridLayoutManager(2, RecyclerView.VERTICAL));
        rv.setAdapter(barangAdapter);

        getData("");

    }

    private void getData(String data) {
        Utils.getData(this, data, new DataResponse() {
            @Override
            public void onSuccess(String data) {
                Log.e("DATA", data);
                parser_Json(data);
                progressBar.setVisibility(View.GONE);
                srl.setRefreshing(false);
            }

            @Override
            public void onError(String data) {
                Toast.makeText(MainActivity.this, "Error fetching data", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                srl.setRefreshing(false);
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    void parser_Json(String data)  {
        ArrayList<Barang> barangs = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jo = jsonArray.getJSONObject(i);
                barangs.add(
                        new Barang(
                                jo.getString("id"),
                                jo.getString("name"),
                                jo.getString("code"),
                                jo.getString("image"),
                                jo.getString("category")
                        )
                );
            }
        }catch (JSONException e) {
            Log.e("JSON", e.getMessage());
        }

        barangAdapter.setData(barangs);
        barangAdapter.notifyDataSetChanged();
        if(barangAdapter.getItemCount() == 0) {
            rv.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
        } else {
            rv.setVisibility(View.VISIBLE);
            empty.setVisibility(View.GONE);
        }



    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public boolean onQueryTextChange(String newText) {
        barangAdapter.setQuery(newText);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(sharedPreferences.getBoolean(Login.login, false)) {
            menu.findItem(R.id.login).setTitle("Logout");
        } else {
            menu.findItem(R.id.login).setTitle("Login");
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return  true;
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_bar_search:
                SearchView search = (SearchView) item.getActionView();
                search.setOnQueryTextListener(this);
                break;
            case R.id.add_item:
                // add item
                startActivity(new Intent(this, AddItem.class));
                break;

            case R.id.login:
                if(sharedPreferences.getBoolean(Login.login, false)) {
                    editor.putBoolean(Login.login, false);
                    editor.apply();
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(this, Login.class));
                }

                break;
        }
        return true;
    }

    @Override
    public void onRefresh() {
        srl.setRefreshing(true);
        barangAdapter.clearAll();
        getData("");
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String data = adapterView.getItemAtPosition(i).toString();
        barangAdapter.clearAll();
        srl.setRefreshing(true);
        if(data.equalsIgnoreCase("All")) {
            getData("");
        } else {
            getData(data);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


}