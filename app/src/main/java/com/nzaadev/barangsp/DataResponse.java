package com.nzaadev.barangsp;

public interface DataResponse {
    void onSuccess(String data);
    void onError(String data);
}
