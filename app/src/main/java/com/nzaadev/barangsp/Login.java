package com.nzaadev.barangsp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    EditText username, password;
    Button doLogin;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static String db = "xxx";
    public static String login = "login";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        doLogin = findViewById(R.id.doLogin);
        sharedPreferences = getSharedPreferences(db, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        doLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String us = username.getText().toString();
                String pw = password.getText().toString();
                if(us.equals("admin") && pw.equals("admin")) {
                    editor.putBoolean(login, true);
                    editor.apply();
                    startActivity(new Intent(Login.this, MainActivity.class));
                    finish();
                } else {
                    Toast.makeText(Login.this, "Gagal...", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}