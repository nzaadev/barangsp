package com.nzaadev.barangsp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class AddItem extends AppCompatActivity  {

    EditText name, code, img;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        name = findViewById(R.id.name);
        code = findViewById(R.id.code);
        img = findViewById(R.id.image);
        save = findViewById(R.id.button);

        save.setOnClickListener(v -> {
            save.setText("Menyimpan bruh...");
            save.setEnabled(false);
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            Utils.postData(this, name.getText().toString(), img.getText().toString(), code.getText().toString(), new DataResponse() {
                @Override
                public void onSuccess(String data) {
                    try {
                        JSONObject js = new JSONObject(data);
                        if(!js.getBoolean("error")) {
                            Toast.makeText(AddItem.this, "Success add data", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AddItem.this, "Error add data", Toast.LENGTH_SHORT).show();
                        }
                        save.setEnabled(true);
                        save.setText("Simpan");
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public void onError(String data) {

                }
            });
        });
    }


}