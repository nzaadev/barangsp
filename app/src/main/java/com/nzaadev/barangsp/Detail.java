package com.nzaadev.barangsp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

public class Detail extends AppCompatActivity {

    public static String DATA = "data";
    Barang barang;
    TextView kode;
    TextView rack;
    TextView nama;
    ImageView gambar;
    String dataBarang;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        sharedPreferences = getSharedPreferences(Login.db, Context.MODE_PRIVATE);

        kode = findViewById(R.id.kodedet);
        rack = findViewById(R.id.rackdet);
        nama = findViewById(R.id.namadet);
        gambar = findViewById(R.id.gambardet);

        Gson gson = new Gson();
        dataBarang = getIntent().getStringExtra(DATA);
        barang = gson.fromJson(dataBarang, Barang.class);
        getSupportActionBar().setTitle("Detail of "+ barang.getName());
//        nama.setText(barang.getNama_barang());
        rack.setText(barang.getId());
        kode.setText(barang.getCode());
        Glide.with(this)
                .load(barang.getImage())
                .into(gambar);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(sharedPreferences.getBoolean(Login.login, false)) {
            menu.findItem(R.id.edit_item).setVisible(true);
        } else {
            menu.findItem(R.id.edit_item).setVisible(false);
        }
        return  true;

    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_item:
                // add item
                Intent intent = new Intent(this, EditItem.class);
                intent.putExtra(DATA, dataBarang);
                startActivity(intent);
                break;
        }
        return true;
    }
}