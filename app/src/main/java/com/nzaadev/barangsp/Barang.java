package com.nzaadev.barangsp;

public class Barang {

    String id;
    String name;
    String code;
    String image;
    String category;

    public Barang(String name, String code, String image, String category) {
        this.name = name;
        this.code = code;
        this.image = image;
        this.category = category;
    }

    public Barang(String id, String name, String code, String image, String category) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.image = image;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getImage() {
        return image;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Barang{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", image='" + image + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
