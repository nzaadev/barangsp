package com.nzaadev.barangsp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class EditItem extends AppCompatActivity {

    EditText name, code, img;
    Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        name = findViewById(R.id.name);
        code = findViewById(R.id.code);
        img = findViewById(R.id.image);
        save = findViewById(R.id.button);

        Gson gson = new Gson();
        Barang barang = gson.fromJson(getIntent().getStringExtra(Detail.DATA), Barang.class);
        name.setText(barang.getName());
        code.setText(barang.code);
        img.setText(barang.getImage());
        String id = barang.getId();
        save.setText("Update");


        save.setOnClickListener(v -> {
            save.setText("Mengupdate bruh...");
            save.setEnabled(false);
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            Utils.updateData(this, id, name.getText().toString(), img.getText().toString(), code.getText().toString(), new DataResponse() {
                @Override
                public void onSuccess(String data) {
                    try {
                        JSONObject js = new JSONObject(data);
                        if(!js.getBoolean("error")) {
                            Toast.makeText(EditItem.this, "Success update data", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EditItem.this, "Error update data", Toast.LENGTH_SHORT).show();
                        }
                        save.setEnabled(true);
                        save.setText("Update");
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public void onError(String data) {

                }
            });
        });
    }
}