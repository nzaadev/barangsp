package com.nzaadev.barangsp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.ViewHolder> {

    ArrayList<Barang> data = new ArrayList<>();
    ArrayList<Barang> dataF = new ArrayList<>();

    public void setData(ArrayList<Barang> data) {
        this.data.addAll(data);
        this.dataF.addAll(data);
    }

    public void clearAll() {
        this.data.clear();
        this.dataF.clear();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.barang_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(dataF.get(position));
    }

    @Override
    public int getItemCount() {
        return dataF.size();
    }


    @SuppressLint("NotifyDataSetChanged")
    public void setQuery(String text) {
        dataF.clear();
        if(text.isEmpty()){
            dataF.addAll(data);
        } else{
            text = text.toLowerCase();
            for(Barang item: data){
                if(item.getName().toLowerCase().contains(text) ||
                        item.getCode().toLowerCase().contains(text)) {
                    dataF.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView code;
        TextView rack;
        TextView name;
        ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            code = itemView.findViewById(R.id.kode);
            rack = itemView.findViewById(R.id.rack);
            name = itemView.findViewById(R.id.nama);
            image = itemView.findViewById(R.id.gambar);
        }

        public void bind(Barang barang){
            code.setText(barang.getCode());
            rack.setText(barang.getId());
            name.setText(barang.getName());
            Glide.with(itemView.getContext())
                    .load(barang.getImage())
                    .into(image);
            itemView.setOnClickListener((v) ->  {
                Gson gson = new Gson();
                Intent i = new Intent(v.getContext(), Detail.class);
                i.putExtra(Detail.DATA, gson.toJson(barang));
                v.getContext().startActivity(i);
            });
        }
    }
}
